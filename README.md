# Mail Relayer

This project is an attempt to simplify relaying incoming emails, e.g. to ActionMailbox.

## Development

1. Install ruby - see `.ruby-version`
2. Install bundler - `gem install bundler`
3. Install dependencies - `bundle install`

## Tests

- `rake test` runs all MiniTest tests in `/test`
- `rubocop` run static code checks

## Build / Release

1. Update version number in `VERSION` if required.
2. `gem build mail-relayer.gemspec`
3. `gem install ./mail-relayer-<VERSION>.gem` to install locally
