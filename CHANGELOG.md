# CHANGELOG

## v0.0.1 - TBD

Initial release.

Contains `Mail::Relayer` class adapted from `ActionMailbox::Relayer` - https://github.com/rails/rails/blob/master/actionmailbox/lib/action_mailbox/relayer.rb
