# frozen_string_literal: true

require "minitest/autorun"
require "webmock/minitest"

class Minitest::Test
  private

  def file_fixture(path)
    File.new "test/fixtures/files/#{path}"
  end
end
