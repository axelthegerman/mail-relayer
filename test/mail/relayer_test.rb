# frozen_string_literal: true

require_relative "../test_helper"

require "mail/relayer"

module Mail
  class RelayerTest < Minitest::Test
    URL = "https://example.com/mail/relay/inbound_emails"
    INGRESS_PASSWORD = "secret"

    def setup
      @relayer = Mail::Relayer.new(url: URL, password: INGRESS_PASSWORD)
    end

    def test_successfully_relaying_an_email
      stub_request(:post, URL).to_return status: 204

      result = @relayer.relay(file_fixture("welcome.eml").read)
      assert_equal "2.0.0", result.status_code
      assert_equal "Successfully relayed message to ingress", result.message
      assert result.success?
      assert !result.failure?

      assert_requested :post, URL, body: file_fixture("welcome.eml").read,
        basic_auth: [ "mail_relayer", INGRESS_PASSWORD ],
        headers: { "Content-Type" => "message/rfc822", "User-Agent" => /\AMail relayer v\d+\./ }
    end

    def test_unsuccessfully_relaying_with_invalid_credentials
      stub_request(:post, URL).to_return status: 401

      result = @relayer.relay(file_fixture("welcome.eml").read)
      assert_equal "4.7.0", result.status_code
      assert_equal "Invalid credentials for ingress", result.message
      assert !result.success?
      assert result.failure?
    end

    def test_unsuccessfully_relaying_due_to_an_unspecified_server_error
      stub_request(:post, URL).to_return status: 500

      result = @relayer.relay(file_fixture("welcome.eml").read)
      assert_equal "4.0.0", result.status_code
      assert_equal "HTTP 500", result.message
      assert !result.success?
      assert result.failure?
    end

    def test_unsuccessfully_relaying_due_to_a_gateway_timeout
      stub_request(:post, URL).to_return status: 504

      result = @relayer.relay(file_fixture("welcome.eml").read)
      assert_equal "4.0.0", result.status_code
      assert_equal "HTTP 504", result.message
      assert !result.success?
      assert result.failure?
    end

    def test_unsuccessfully_relaying_due_to_ECONNRESET
      stub_request(:post, URL).to_raise Errno::ECONNRESET.new

      result = @relayer.relay(file_fixture("welcome.eml").read)
      assert_equal "4.4.2", result.status_code
      assert_equal "Network error relaying to ingress: Connection reset by peer", result.message
      assert !result.success?
      assert result.failure?
    end

    def test_unsuccessfully_relaying_due_to_connection_failure
      stub_request(:post, URL).to_raise SocketError.new("Failed to open TCP connection to example.com:443")

      result = @relayer.relay(file_fixture("welcome.eml").read)
      assert_equal "4.4.2", result.status_code
      assert_equal "Network error relaying to ingress: Failed to open TCP connection to example.com:443", result.message
      assert !result.success?
      assert result.failure?
    end

    def test_unsuccessfully_relaying_due_to_client_side_timeout
      stub_request(:post, URL).to_timeout

      result = @relayer.relay(file_fixture("welcome.eml").read)
      assert_equal "4.4.2", result.status_code
      assert_equal "Timed out relaying to ingress", result.message
      assert !result.success?
      assert result.failure?
    end

    def test_unsuccessfully_relaying_due_to_an_unhandled_exception
      stub_request(:post, URL).to_raise StandardError.new("Something went wrong")

      result = @relayer.relay(file_fixture("welcome.eml").read)
      assert_equal "4.0.0", result.status_code
      assert_equal "Error relaying to ingress: Something went wrong", result.message
      assert !result.success?
      assert result.failure?
    end
  end
end
