# frozen_string_literal: true

version = File.read(File.expand_path("VERSION", __dir__)).strip

Gem::Specification.new do |s|
  s.name        = "mail-relayer"
  s.version     = version
  s.date        = "2019-12-16"
  s.summary     = "Extracts ActionMailer::Relayer for stand-alone use"
  s.description = "A small wrapper around ActionMailer::Relayer without any dependencies."
  s.authors     = ["Axel Gustav"]
  s.email       = "dev@axelgustav.de"
  s.files       = Dir.glob("lib/**/*") + %w[VERSION CHANGELOG.md LICENSE README.md]
  s.test_files  = Dir.glob("spec/**/*")
  s.homepage    = "https://rubygems.org/gems/mail-relayer"
  s.license     = "MIT"

  s.required_ruby_version = ">= 2.5.0"

  s.add_development_dependency("rake", ">= 1")
  s.add_development_dependency("rubocop", "~> 0.77.0")

  s.add_development_dependency("minitest", "~> 5.13")

  s.add_development_dependency("webmock", "~> 3.7")
end
